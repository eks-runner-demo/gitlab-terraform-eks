resource "helm_release" "gitlab_runner" {
  name = "gitlab-runner"

  repository = "https://charts.gitlab.io"
  chart      = "gitlab-runner"

  create_namespace = true
  namespace        = var.runner_namespace

  set {
    name  = "gitlabUrl"
    value = var.gitlab_url
  }

  set {
    name  = "runnerRegistrationToken"
    value = data.gitlab_group.this.runners_token
  }

  set {
    name  = "rbac.create"
    value = true
  }

  set {
    name  = "runner.config"
    value = file("${path.root}/.gitlab/runner/config")
  }
}
