terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }
}
